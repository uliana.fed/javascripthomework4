let giveFirstNumber 
do {
   giveFirstNumber = prompt("Введіть перше число:");
} while(isNaN(giveFirstNumber));

let giveSecondNumber 
do {
   giveSecondNumber = prompt("Введіть друге число:");
} while(isNaN(giveSecondNumber));

let operator
do {
    operator = prompt("Введіть арифметичний оператор (+, -, *, /):")
} while (!["+","-","*","/"].includes(operator))

function calc(giveFirstNumber, giveSecondNumber, operator) {
    let result;
    switch(operator) {
        case "+":
            result = giveFirstNumber + giveSecondNumber;
            break;
        case "-":
            result = giveFirstNumber - giveSecondNumber;
            break;
        case "/":
            result = giveFirstNumber / giveSecondNumber;
            break;
        case "*":
            result = giveFirstNumber * giveSecondNumber;
            break;
        default:
            result = "Ой-ой! Помилка. Спробуйте ще раз."
    }
    return result;
}

console.log(`Відповідь: ${giveFirstNumber} ${operator} ${giveSecondNumber} =`)
console.log(calc(Number(giveFirstNumber), Number(giveSecondNumber),operator))